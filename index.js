const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const axios = require('axios');
const app = express();
const PORT = process.env.PORT || 3000;
const API_URL = 'https://us-central1-kivson.cloudfunctions.net/charada-aleatoria'

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const printJoke = () => axios.get(API_URL);

app.get('/', (req, res) => {
  printJoke()
    .then(({ data }) => {
      res.render('home', { joke: data });
    })
    .catch(err => console.log(err))
});


app.listen(PORT, () => console.log(`App rodando na porta ${PORT}`));